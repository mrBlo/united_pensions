package com.iam.exception;

public class ResourceNotFound extends RuntimeException{
    /** Default Serial Version UID*/
    private static final long serialVersionUID = 1L;

    public ResourceNotFound(long ResourceId) {
        super("Resource with id [ "+ResourceId+" ] not found");
    }
}
