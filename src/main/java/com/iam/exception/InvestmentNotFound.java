package com.iam.exception;

public class InvestmentNotFound extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public InvestmentNotFound() {
        super("Investment not found");
    }
}