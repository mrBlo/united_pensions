package com.iam.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ClientNotFound.class)
    public ResponseEntity<APIError> handleClientNotFoundException(ClientNotFound ex, WebRequest request) {
        APIError errorDetails = new APIError(HttpStatus.NOT_FOUND.value(), ex.getMessage(), ex.getLocalizedMessage(),
                request.getDescription(false) + "", LocalDateTime.now());
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ClientAlreadyExists.class)
    public ResponseEntity<APIError> handleClientAlreadyExistsException(ClientAlreadyExists ex, WebRequest request) {
        APIError errorDetails = new APIError(HttpStatus.CONFLICT.value(), ex.getMessage(), ex.getLocalizedMessage(),
                request.getDescription(false) + "", LocalDateTime.now());
        return new ResponseEntity<>(errorDetails, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(InvestmentNotFound.class)
    public ResponseEntity<APIError> handleInvestmentNotFoundException(InvestmentNotFound ex, WebRequest request) {
        APIError errorDetails = new APIError(HttpStatus.NOT_FOUND.value(), ex.getMessage(), ex.getLocalizedMessage(),
                request.getDescription(false) + "", LocalDateTime.now());
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvestmentAlreadyExists.class)
    public ResponseEntity<APIError> handleInvestmentAlreadyExistsException(InvestmentAlreadyExists ex, WebRequest request) {
        APIError errorDetails = new APIError(HttpStatus.CONFLICT.value(), ex.getMessage(), ex.getLocalizedMessage(),
                request.getDescription(false) + "", LocalDateTime.now());
        return new ResponseEntity<>(errorDetails, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ResourceNotFound.class)
    public ResponseEntity<APIError> handleResourceNotFoundException(ResourceNotFound ex, WebRequest request) {
        APIError errorDetails = new APIError(HttpStatus.NOT_FOUND.value(), ex.getMessage(), ex.getLocalizedMessage(),
                request.getDescription(false) + "", LocalDateTime.now());
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

}