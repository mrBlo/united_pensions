package com.iam.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ClientAlreadyExists extends RuntimeException {
    /** Default Serial Version UID*/
    private static final long serialVersionUID = 1L;

    public ClientAlreadyExists() {
        super("Client already exists.");
    }
}