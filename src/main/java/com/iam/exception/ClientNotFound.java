package com.iam.exception;

public class ClientNotFound  extends RuntimeException{
    /** Default Serial Version UID*/
    private static final long serialVersionUID = 1L;

    public ClientNotFound() {
        super("Client not found");
    }

    public ClientNotFound(long clientId) {
        super("Client with id [ "+clientId+" ] not found");
    }
}