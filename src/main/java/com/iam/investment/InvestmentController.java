package com.iam.investment;

import com.iam.client.ClientRepository;
import com.iam.exception.ClientNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "https://iamupt.pages.dev/")
public class InvestmentController {
    private final InvestmentService investmentService;
    private final ClientRepository clientRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(InvestmentController.class);

    public InvestmentController(InvestmentService investmentService, ClientRepository clientRepository) {
        this.investmentService = investmentService;
        this.clientRepository = clientRepository;
    }

    @GetMapping(value = "/investments")
    public Investments getAllInvestments() {
        LOGGER.info("GETTING ALL INVESTMENTS");
        return investmentService.getAllInvestments();
    }

    @GetMapping(value = "/investments/total")
    public int getTotalNumberOfInvestments() {
        LOGGER.info("GETTING TOTAL NUMBER OF ALL INVESTMENTS");
        List<InvestmentDto> allInvestments = investmentService.getAllInvestments().getInvestments();
        return allInvestments.size();
    }

    @GetMapping("/investments/{id}")
    @ResponseStatus(HttpStatus.OK)
    public InvestmentDto getInvestmentById(@PathVariable(value = "id") Long id) {
        return investmentService.findInvestmentById(id);
    }

    //GET THE LATEST INTEREST
    @GetMapping("/investments/{investmentId}/latest-interest")
    public ResponseEntity<Object> getLatestInterestByInvestmentId(@PathVariable(value = "investmentId") Long investmentId){
        LOGGER.info("GETTING LATEST INTEREST OF INVESTMENT [ID="+investmentId+"]");
        InvestmentDto foundInvestment = investmentService.findInvestmentById(investmentId);
//        if (foundInvestment.getTerminatedOn()!=null){ return new ResponseEntity<>("Investment has been terminated",HttpStatus.EXPECTATION_FAILED); }
        Investment investmentWithLatestInterest = investmentService.getInvestmentWithLatestInterest(foundInvestment);
        BigDecimal interest = investmentWithLatestInterest.getInterest();
        LOGGER.info("LATEST INTEREST OF INVESTMENT [ID="+investmentId+"] = "+interest);
        return new ResponseEntity<>(interest, HttpStatus.OK);
    }

    @GetMapping("/clients/{clientId}/investments")
    public ResponseEntity<Investments> getAllInvestmentsByClientId(@PathVariable(value = "clientId") Long clientId) {
        if (!clientRepository.existsById(clientId)) {
            throw new ClientNotFound(clientId);
        }
        Investments allInvestmentsByClientId = investmentService.findAllInvestmentsByClientId(clientId);
        return new ResponseEntity<>(allInvestmentsByClientId, HttpStatus.OK);
    }

    //GET ALL PRINCIPALS OF ONE CLIENT
    @GetMapping("/clients/{clientId}/principals")
    public ResponseEntity<Object> getAllPrincipalsByClientId(@PathVariable(value = "clientId") Long clientId){
        if (!clientRepository.existsById(clientId)) {
            throw new ClientNotFound(clientId);
        }
        Principals allPrincipalsByClientId = investmentService.findAllPrincipalsByClientId(clientId);
        return new ResponseEntity<>(allPrincipalsByClientId, HttpStatus.OK);
    }

    //GET SUM OF ALL PRINCIPALS OF ONE CLIENT
    @GetMapping("/clients/{clientId}/total-principal")
    public ResponseEntity<Object> getTotalPrincipalsByClientId(@PathVariable(value = "clientId") Long clientId){
        if (!clientRepository.existsById(clientId)) {
            throw new ClientNotFound(clientId);
        }
        BigDecimal totalPrincipalsOfClient = investmentService.getTotalPrincipalsOfClientByClientId(clientId);
        return new ResponseEntity<>(totalPrincipalsOfClient, HttpStatus.OK);
    }

    //GET SUM OF ALL INTERESTS OF ONE CLIENT
    @GetMapping("/clients/{clientId}/total-interest")
    public ResponseEntity<Object> getTotalInterestsByClientId(@PathVariable(value = "clientId") Long clientId){
        if (!clientRepository.existsById(clientId)) {
            throw new ClientNotFound(clientId);
        }
        BigDecimal totalInterestsOfClient = investmentService.getTotalInterestsOfClientByClientId(clientId);
        return new ResponseEntity<>(totalInterestsOfClient, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/clients/{clientId}/investments", consumes = MediaType.APPLICATION_JSON_VALUE)
    public InvestmentDto addInvestment(@PathVariable(value = "clientId") Long clientId, @Valid @RequestBody InvestmentDto investmentDto) {
        LOGGER.info("INITIATING CREATION OF INVESTMENT [DETAILS= " + investmentDto + "]");
        InvestmentDto newInvestmentDto = investmentService.createInvestment(clientId, investmentDto);
        LOGGER.info("INVESTMENT [DETAILS= " + newInvestmentDto + "] SAVED SUCCESSFULLY");
        return newInvestmentDto;
    }

}
