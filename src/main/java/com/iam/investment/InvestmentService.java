package com.iam.investment;

import com.iam.calculator.InterestCalculator;
import com.iam.calculator.PrincipalCalculator;
import com.iam.client.ClientRepository;
import com.iam.exception.ClientNotFound;
import com.iam.exception.InvestmentNotFound;
import com.iam.exception.ResourceNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InvestmentService {
    private final InvestmentRepository investmentRepository;
    private final ClientRepository clientRepository;
    private final InvestmentMapper investmentMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(InvestmentService.class);

    public InvestmentService(InvestmentRepository investmentRepository, ClientRepository clientRepository, InvestmentMapper investmentMapper) {
        this.investmentRepository = investmentRepository;
        this.clientRepository = clientRepository;
        this.investmentMapper = investmentMapper;
    }

    public Investments getAllInvestments() {
        List<InvestmentDto> allInvestments = new ArrayList<>();
        investmentRepository.findAll().forEach(investment -> {
           allInvestments.add( investmentMapper.convertToDto(investment));
                });
        Investments investments = new Investments();
        investments.setInvestments(allInvestments);
        return investments;
    }

    public InvestmentDto findInvestmentById(long id) {
        Optional<Investment> optionalInvestment = investmentRepository.findById(id);
        if (optionalInvestment.isPresent()) {
            LOGGER.info(String.format("INVESTMENT [ID= %d] FOUND", id));
            return investmentMapper.convertToDto(optionalInvestment.get());
        }
        LOGGER.warn("INVESTMENT [ID= " + id + "] NOT FOUND");
        throw new InvestmentNotFound();
    }

    public Investments findAllInvestmentsByClientId(long clientId) {
        List<Investment> investmentList = investmentRepository.findByClientId(clientId);
        if (!investmentList.isEmpty()) {
            //convert list of investments to list of investmentDTOs
            List<InvestmentDto> investmentDtoList = investmentList
                    .stream()
                    .map(investmentMapper::convertToDto)
                    .collect(Collectors.toList());
            Investments investments = new Investments();
            investments.setInvestments(investmentDtoList);
            LOGGER.info("INVESTMENTS [CLIENT_ID= " + clientId + "] FOUND");
            return investments;
        }
        LOGGER.warn("INVESTMENTS [CLIENT_ID= " + clientId + "] NOT FOUND");
        throw new InvestmentNotFound();
    }

    public Principals findAllPrincipalsByClientId(long clientId) {
        List<BigDecimal> principalList = investmentRepository.findPrincipalByClientId(clientId);
        if (!principalList.isEmpty()) {
            Principals principals = new Principals();
            principals.setPrincipals(principalList);
            LOGGER.info("ALL PRINCIPAL AMOUNTS [CLIENT_ID= " + clientId + "] FOUND");
            LOGGER.info("PRINCIPAL AMOUNTS OF CLIENT [ID: " + clientId + "] => " + principalList);
            return principals;
        }
        LOGGER.warn("PRINCIPAL AMOUNTS [CLIENT_ID= " + clientId + "] NOT FOUND");
        throw new ResourceNotFound(clientId);
    }

    public BigDecimal getTotalPrincipalsOfClientByClientId(long clientId) {
        BigDecimal sum = BigDecimal.ZERO;
        List<BigDecimal> principalList = investmentRepository.findPrincipalByClientId(clientId);
        LOGGER.info("PRINCIPAL AMOUNTS OF CLIENT [ID: " + clientId + "] => " + principalList);
        for (int i = 0; i < principalList.size(); i++) {
            sum = sum.add(principalList.get(i));
        }
        return sum;
    }

    public BigDecimal getTotalInterestsOfClientByClientId(long clientId) {
        // Get all investments of client
        List<Investment> investmentList = investmentRepository.findByClientId(clientId);
        if (investmentList.isEmpty()) {
            LOGGER.warn("INVESTMENTS [CLIENT_ID= " + clientId + "] NOT FOUND");
            throw new InvestmentNotFound();
        }
        BigDecimal sumOfAllInterests = investmentList.stream()
                // Filter out to remove all terminated investments.(All terminated investments are not null)
                .filter(investment -> investment.getTerminatedOn() == null)
                // compute and return interest of investment
                .map(
                        investment -> {
                            // compute interest of investment
                            //--> Change Check Date back to plusDays(200) for demo purposes
                            InterestCalculator interestCalculator = new InterestCalculator(investment.getPrincipal(),
                                    investment.getRatePercentage(), investment.getCreatedOn(), LocalDate.now());
                            BigDecimal computedInterest = interestCalculator.compute();
                            // set interest
                            investment.setInterest(computedInterest);
                            //get interest amount
                            return investment.getInterest();
                        }
                )
                // add all interest amounts
                .reduce(BigDecimal.valueOf(0), BigDecimal::add);
        LOGGER.info("TOTAL SUM OF INTEREST OF CLIENT [ID: " + clientId + "] => " + sumOfAllInterests);
        return sumOfAllInterests;
    }

    public Investment getInvestmentWithLatestInterest(InvestmentDto investmentDto){
        Investment investment = investmentMapper.convertToEntity(investmentDto);
        //calculate interest using InterestCalculator
        //--> Change Check Date back to plusDays(200) for demo purposes
        investment.setCheckedOn(LocalDate.now());
        InterestCalculator interestCalculator = new InterestCalculator(
                investment.getPrincipal(),
                investment.getRatePercentage(),
                investment.getCreatedOn(),
                investment.getCheckedOn()
        );
        BigDecimal latestInterest = interestCalculator.compute();
        //set interest of investment using your latest interest result
        investment.setInterest(latestInterest);
        return investment;
    }

    public InvestmentDto createInvestment(long clientId, InvestmentDto investmentDto) {
        Investment mappedInvestment = investmentMapper.convertToEntity(investmentDto);
        Investment createdInvestment = clientRepository.findById(clientId)
                .map(client -> {
                    mappedInvestment.setClient(client);
                    //compute principal
                    PrincipalCalculator principalCalculator = new PrincipalCalculator(client.getSalary(), client.getRate());
                    mappedInvestment.setPrincipal(principalCalculator.compute());
                    mappedInvestment.setRatePercentage(investmentDto.getInterestRate());
                    mappedInvestment.setMaturedOn(this.computeMaturityDate());
                    mappedInvestment.setInterest(BigDecimal.ZERO);
                    return investmentRepository.save(mappedInvestment);
                })
                .orElseThrow(() -> new ClientNotFound(clientId));
        //convert investment to Dto
        return investmentMapper.convertToDto(createdInvestment);
    }

    public boolean doesInvestmentExist(long investmentId) {
        Optional<Investment> optionalInvestment = investmentRepository.findById(investmentId);
        return optionalInvestment.isPresent();
    }

    /* Other Methods */

    //compute maturityDate by adding a year to creationDate
    public LocalDate computeMaturityDate() {
        LocalDate today = LocalDate.now();
        return today.plusYears(1);
    }

}
