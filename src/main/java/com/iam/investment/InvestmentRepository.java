package com.iam.investment;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface InvestmentRepository extends CrudRepository<Investment, Long> {
    List<Investment> findByClientId(long clientId);

    @Query(value = "SELECT PRINCIPAL FROM investment WHERE client_id = ?1",nativeQuery =true)
    List<BigDecimal> findPrincipalByClientId(long clientId);
}
