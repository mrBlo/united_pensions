package com.iam.investment;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
public class InvestmentDto {
    private Long id;
    private String clientName;
    private BigDecimal clientSalary;
    private double clientRate;
    private BigDecimal principal;
    private BigDecimal interestRate;
    private LocalDate createdOn;
    private LocalDate maturedOn;
    private BigDecimal interest;

}
