package com.iam.investment;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * This class holds a list of principal amounts
 */

@Data
public class Principals {
    List<BigDecimal> Principals;
}
