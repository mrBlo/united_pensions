package com.iam.investment;

import com.iam.client.Client;
import org.springframework.stereotype.Component;

@Component
public class InvestmentMapper {
    public InvestmentDto convertToDto(Investment investment) {
        return InvestmentDto.builder()
                .id(investment.getId())
                .clientName(investment.getClient().getName())
                .clientSalary(investment.getClient().getSalary())
                .clientRate(investment.getClient().getRate())
                .principal(investment.getPrincipal())
                .interestRate(investment.getRatePercentage())
                .createdOn(investment.getCreatedOn())
                .maturedOn(investment.getMaturedOn())
                .interest(investment.getInterest())
                .build();
    }

    public Investment convertToEntity(InvestmentDto investmentDto) {
        Client client = new Client(investmentDto.getClientName(), investmentDto.getClientSalary(),
                investmentDto.getClientRate());

        return new Investment(
                client,
                investmentDto.getPrincipal(),
                investmentDto.getInterestRate(),
                investmentDto.getCreatedOn(),
                investmentDto.getMaturedOn(),
                investmentDto.getInterest());
    }

}
