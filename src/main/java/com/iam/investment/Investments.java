package com.iam.investment;

import lombok.Data;
import java.util.List;

@Data
public class Investments {
    List<InvestmentDto> Investments;
}
