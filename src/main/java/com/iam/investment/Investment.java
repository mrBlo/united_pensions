package com.iam.investment;

import com.iam.client.Client;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table
//@AllArgsConstructor
//@RequiredArgsConstructor // creates a constructor with fields which are annotated by @NonNull annotation.
@NoArgsConstructor
@Setter
@Getter
@ToString
@EntityListeners(AuditingEntityListener.class)
public class Investment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;
    private BigDecimal principal;
    private BigDecimal ratePercentage;

    @CreatedDate
    private LocalDate createdOn;
    private LocalDate maturedOn;
    private LocalDate terminatedOn;
    @CreatedDate
    private LocalDate checkedOn;
    private BigDecimal interest;

    public Investment(Client client, BigDecimal principal, BigDecimal ratePercentage,
                      LocalDate createdOn, LocalDate maturedOn, BigDecimal interest) {
        this.client = client;
        this.principal = principal;
        this.ratePercentage = ratePercentage;
        this.createdOn = createdOn;
        this.maturedOn = maturedOn;
        this.interest = interest;
    }
}
