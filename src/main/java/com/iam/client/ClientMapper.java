package com.iam.client;

import org.springframework.stereotype.Component;

@Component
public class ClientMapper {
    public ClientDto convertToDto(Client client) {
        return ClientDto.builder()
                .id(client.getId())
                .name(client.getName())
                .salary(client.getSalary())
                .rate(client.getRate())
                .build();
    }

    public Client convertToEntity(ClientDto clientDto) {
        return new Client(clientDto.getName(),
                clientDto.getSalary(),
                clientDto.getRate());
    }
}
