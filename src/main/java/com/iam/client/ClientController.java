package com.iam.client;

import com.iam.exception.ClientAlreadyExists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "https://iamupt.pages.dev/")
public class ClientController {
    private final ClientService clientService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    public ClientController(ClientService clientService){
        this.clientService= clientService;
    }

    @GetMapping(value = "/clients")
    public Clients getAll() {
        LOGGER.info("GETTING ALL CLIENTS");
        return clientService.getAllClients();
    }

    @GetMapping(value = "/clients/total")
    public int getNumberOfAllClients() {
        LOGGER.info("GETTING TOTAL NUMBER OF ALL CLIENTS");
        List<ClientDto> clientList= clientService.getAllClients().getClients();
        return clientList.size();
    }

    @GetMapping(value = "/clients/{id}")
    public ResponseEntity<Object> getClient(@PathVariable(value = "id") Long clientId){
        LOGGER.info("GETTING CLIENT [ID= " + clientId + "]");
        ClientDto foundClient =  clientService.findClientById(clientId);
            return new ResponseEntity<>(foundClient, HttpStatus.OK);
    }

    @PostMapping(value = "/clients", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> post(@Valid @RequestBody ClientDto clientDto) {
        LOGGER.info("INITIATING CREATION OF CLIENT [DETAILS= " + clientDto + "]");

        if (clientService.doesClientExist(clientDto.getName())) {
            LOGGER.warn("CLIENT WITH NAME [ " + clientDto.getName()+ " ] ALREADY EXISTS");
            throw new ClientAlreadyExists();
        }
        ClientDto savedClient= clientService.createClient(clientDto);
        LOGGER.info("CLIENT [DETAILS= " + savedClient + "] SAVED SUCCESSFULLY");
        return new ResponseEntity<>(savedClient, HttpStatus.CREATED);
    }

}
