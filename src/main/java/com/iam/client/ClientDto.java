package com.iam.client;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ClientDto {
    private Long id;
    private String name;
    private BigDecimal salary;
    private double rate;
}
