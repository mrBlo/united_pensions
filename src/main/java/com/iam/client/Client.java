package com.iam.client;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table
@NoArgsConstructor
@RequiredArgsConstructor // creates a constructor with fields which are annotated by @NonNull annotation.
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class) //to allow usage of @CreatedDate and @LastModifiedDate
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull private String name;
    @CreatedDate
    private LocalDate createdOn;
    @LastModifiedDate
    private LocalDate updatedOn;
    @NonNull private BigDecimal salary;
    @NonNull private double rate;

}
