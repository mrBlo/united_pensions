package com.iam.client;

import com.iam.exception.ClientNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientService.class);

    public ClientService(ClientRepository clientRepository, ClientMapper clientMapper) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
    }

    public Clients getAllClients() {
        List<ClientDto> allClients = new ArrayList<>();
        clientRepository.findAll().forEach(client -> {
                    allClients.add(clientMapper.convertToDto(client));
                }
        );
        Clients clients = new Clients();
        clients.setClients(allClients);
        return clients;
    }

    public ClientDto findClientById(long id) {
        Optional<Client> optionalClient = clientRepository.findById(id);
        if (optionalClient.isPresent()) {
            LOGGER.info(String.format("CLIENT [ID= %d] FOUND", id));
            return clientMapper.convertToDto(optionalClient.get());
        }
        LOGGER.warn("PRODUCT [ID= " + id + "] NOT FOUND");
        throw new ClientNotFound();
    }

    public ClientDto createClient(ClientDto clientDto) {
        Client client = clientMapper.convertToEntity(clientDto);
        ClientDto savedClientDto = clientMapper.convertToDto(clientRepository.save(client));
        return savedClientDto;
    }

    public boolean doesClientExist(String clientName) {
        Optional<Client> optionalClient = clientRepository.findByName(clientName);
        return optionalClient.isPresent();
    }


}
