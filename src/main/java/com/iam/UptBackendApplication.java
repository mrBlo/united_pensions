package com.iam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing //to allow usage of @CreatedDate etc.
public class UptBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UptBackendApplication.class, args);
    }

}
