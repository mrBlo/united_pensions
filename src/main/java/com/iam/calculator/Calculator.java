package com.iam.calculator;

import java.math.BigDecimal;

public interface Calculator {

    BigDecimal compute();
}
