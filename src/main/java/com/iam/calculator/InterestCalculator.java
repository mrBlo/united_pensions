package com.iam.calculator;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;

/**
 * This class calculates Simple Interest
 * i.e. SI = (PRT)/100
 * eg. R= 10% , T = 45days, P = 20
 * => SI = 20 * (10/100) * (45/365)
 */

@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class InterestCalculator implements Calculator {

    private BigDecimal principal, ratePercent;
    private LocalDate creationDate, checkDate;
    private final int NUMBER_OF_DAYS_IN_YEAR = 365;

    @Override
    public BigDecimal compute() {
        BigDecimal time = BigDecimal.valueOf(calculateTime());
        BigDecimal rateMultiplyTime = calculateRate().multiply(time);
        //round interest to 2 decimal places
        BigDecimal interest = principal.multiply(rateMultiplyTime).setScale(2,RoundingMode.HALF_UP);
        return interest;
    }

    /**
     * period = diff btwn creationDate and checkDate
     * NB: period is in days
     */
    public long calculatePeriodInDays() {
        return DAYS.between(creationDate, checkDate);
    }


    /**
     * calculate Time to be used in formula
     * eg. 31/365
     * returns double
     */
    public double calculateTime() {
        // convert result to double
        return (double) calculatePeriodInDays() / NUMBER_OF_DAYS_IN_YEAR;
    }


    /**
     * calculate rate to be used in formula
     * eg. 15/100
     */
    public BigDecimal calculateRate() {
        // round rate to scale 2
        return this.ratePercent.divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
    }


}
