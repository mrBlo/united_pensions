package com.iam.calculator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * This class calculates principal amount
 * ie.Principal = salary*(investmentPercentage/100)
 * eg. 10% of monthly salary 1000 cedis
 * => Principal = 1000 * (10/100)
 */
@Setter
@AllArgsConstructor
public class PrincipalCalculator implements Calculator {

    private BigDecimal salary;
    private double salaryPercentage;

    @Override
    public BigDecimal compute() {
        // divide salaryPercentage by 100
        BigDecimal salaryRate= BigDecimal.valueOf(salaryPercentage/100);
        return salary.multiply(salaryRate);
    }
}
