package com.iam.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Running Interest Calculator Tests")
class InterestCalculatorTest {

    InterestCalculator interestCalculator;
    @BeforeEach
    void setUp() {
        interestCalculator = new InterestCalculator();
        interestCalculator.setPrincipal(BigDecimal.valueOf(400));
        interestCalculator.setCreationDate(LocalDate.now());
    }

    @DisplayName("Running Compute Method")
    @Test
    void compute() {
        interestCalculator.setCheckDate(LocalDate.now().plusDays(2));
        interestCalculator.setRatePercent(BigDecimal.TEN);
        assertAll(
                ()->assertFalse(interestCalculator.compute().equals(0.22),()->"Should return Big Decimal not double"),
                ()-> assertEquals(interestCalculator.compute(), BigDecimal.valueOf(0.22)));
    }

    // CALCULATE PERIOD IN DAYS TESTS GROUPED IN NESTED CLASS
    @Nested
    @DisplayName("Calculate Period Subclass")
    class CalculatePeriodInDays {

        @DisplayName("Running Negative CalculatePeriodInDays")
        @Test
        void testNegativePeriodInDays() {
            interestCalculator.setCheckDate(LocalDate.now().minusDays(2));
            interestCalculator.setRatePercent(BigDecimal.TEN);
            assertEquals(-2,interestCalculator.calculatePeriodInDays());
        }

        @DisplayName("Running Zero CalculatePeriodInDays")
        @Test
        void testZeroPeriodInDays() {
            interestCalculator.setCheckDate(LocalDate.now());
            interestCalculator.setRatePercent(BigDecimal.TEN);
            assertEquals(0,interestCalculator.calculatePeriodInDays());
        }

        @DisplayName("Running Positive CalculatePeriodInDays")
        @Test
        void testPositivePeriodInDays() {
            interestCalculator.setCheckDate(LocalDate.now().plusDays(5));
            interestCalculator.setRatePercent(BigDecimal.TEN);
            assertEquals(5,interestCalculator.calculatePeriodInDays());
        }
    }

    // CALCULATE PERIOD IN DAYS TESTS GROUPED IN NESTED CLASS
    @Nested
    @DisplayName("Calculate Time Subclass")
    class CalculateTime {

        @DisplayName("Running when Check and Creation Date are the same")
        @Test
        void testSameCheckAndCreationDate() {
            interestCalculator.setCheckDate(LocalDate.now());
            interestCalculator.setRatePercent(BigDecimal.TEN);
            assertEquals(interestCalculator.calculateTime(), 0);
        }
    }

}